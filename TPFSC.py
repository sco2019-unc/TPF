#!/usr/bin/python3
import mysql.connector
import RPi.GPIO as GPIO
import time
from pad4pi import rpi_gpio
from RPLCD.i2c import CharLCD

#******************************************#

mydb = mysql.connector.connect(
  host="localhost",
  user="webuser",
  passwd="webpassword",
  database="cerradura"
)

cursor = mydb.cursor(prepared=True)
cursor2 = mydb.cursor()
sql_select_query = """select ID from Usuarios where Password = %s"""
sql_insert_query = """insert into HistorialEventos (ID_Evento, ID_Usuario) values (%s, %s)"""

lcd = CharLCD('PCF8574', 0x27)
lcd.write_string('INSERTAR CLAVE:')
claveIntroducida = []
pos = 0
intentos = 0
estado = 0

#******************************************#

def convert(list):
    s = [str(i) for i in list]		# Converting integer list to string list
    res = str("".join(s))		# Join list items using join()

    return(res)

#******************************************#

def printKey(key):
    global pos
    lcd.cursor_pos = (1, pos)           # Posiciono el cursor del display
    lcd.write_string(key)               # Escribo el digito precionado
    claveIntroducida.append(key)        # Almaceno el digito junto a los otros
    pos += 1                            # Aumento posicion para desplazar cursor

#******************************************#

def moverPestillo(p):
    global estado

    if estado == 0:
        p.ChangeDutyCycle(4.5)    	# Pulso de 4.5% para girar el servo hacia la derecha
        estado = 1			# Cambio el estado
    else:
        p.ChangeDutyCycle(10.5)		# Pulso de 10.5% para girar el servo hacia la izq
        estado = 0			# Cambio el estado

#******************************************#

KEYPAD = [
    ["1", "2", "3", "A"],
    ["4", "5", "6", "B"],
    ["7", "8", "9", "C"],
    ["*", "0", "#", "D"]
]

COL_PINS = [17, 15, 14, 4] 		# Defino pines de columnas
ROW_PINS = [24,22,27,18] 		# Defino pines de filas

factory = rpi_gpio.KeypadFactory()
keypad = factory.create_keypad(keypad=KEYPAD, row_pins=ROW_PINS, col_pins=COL_PINS)
keypad.registerKeyPressHandler(printKey) # Registro el handler de interrupcion de teclado

#******************************************#

def main():
  global mydb
  global pm
  global system_sts
  global intentos
  global pos

  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BCM)       		# Use BCM GPIO numbers

  GPIO.setup(26,GPIO.OUT)		# Pin 26 como salida para el servo
  GPIO.setup(19,GPIO.OUT)		# Pin 19 como salida para el buzzer
  GPIO.setup(13,GPIO.OUT)		# Pin 13 como salida para el led rojo
  GPIO.setup(6,GPIO.OUT)    		# Pin 6 como salida para el led verde

  GPIO.output(13,GPIO.LOW)		# Apago led rojo
  GPIO.output(6,GPIO.LOW)		# Apago led verde
  GPIO.output(19,GPIO.LOW)		# Apago buzzer

  p = GPIO.PWM(26,50)        		# Pin 26 en modo PWM y enviamos 50 pulsos por seg
  p.start(10.5)               		# Enviamos un pulso del 7.5% para centrar el servo

  while True:
      if (len(claveIntroducida) == 4):	# Pregunto si se introdujo clave completa
        intentos += 1			# Aumento contador de intentos
        lcd.clear()                     # Limpio el display
        keypad.unregisterKeyPressHandler(printKey)
        cursor.execute(sql_select_query, (convert(claveIntroducida), ))
        idUsuario = cursor.fetchall()

        if cursor.rowcount == 1:	# Me fijo si es una clave valida
            intentos = 0		# Reseteo el contador de intentos
            GPIO.output(6,GPIO.HIGH) 	# Prendo led verde
            moverPestillo(p)		# Trabo o destrabo la cerradura
            lcd.write_string('CLAVE VALIDA')
            lcd.cursor_pos = (1, 0)     # Cambio de linea
            if estado == 0:
                cursor2.execute(sql_insert_query, (1, convert(idUsuario[0])))
                lcd.write_string('ABRIENDO CERROJO')
            else:
                cursor2.execute(sql_insert_query, (2, convert(idUsuario[0])))
                lcd.write_string('TRABANDO CERROJO')
            time.sleep(3)		# Espero
            lcd.clear()                 # Limpio el display
            GPIO.output(6,GPIO.LOW) 	# Apago led verde
        else:
            GPIO.output(13,GPIO.HIGH) 	# Prendo led rojo
            cursor2.execute(sql_insert_query, (4, 'NULL'))
            lcd.write_string('CLAVE INVALIDA')
            lcd.cursor_pos = (1, 0)     # Cambio de linea
            lcd.write_string('INTENTO GRABADO')
            time.sleep(3)		# Espero
            lcd.clear()                 # Limpio el display
            GPIO.output(13,GPIO.LOW) 	# Apago led rojo

        if intentos == 3:
            GPIO.output(19,GPIO.HIGH)   # Prendo buzzer
            intentos = 0                # Reseteo el contador de intentos
            cursor2.execute(sql_insert_query, (3, 'NULL'))
            lcd.write_string('ACTIVIDAD ILEGAL')
            lcd.cursor_pos = (1, 0)     # Cambio de linea
            lcd.write_string('GUARDIA ALERTADO')
            time.sleep(5)               # Espero
            lcd.clear()                 # Limpio el display
            GPIO.output(19,GPIO.LOW)    # Apago buzzer

        mydb.commit()                   # Guardo el log en la base de datos
        del claveIntroducida[:]		# Borro clave introducida para leer proxima clave
        pos = 0                         # Vuelvo posicion a 0
        lcd.write_string('INSERTAR CLAVE:')
        keypad.registerKeyPressHandler(printKey)

#******************************************#

if __name__ == '__main__':

  try:
    main()

  except KeyboardInterrupt:
    pass

  finally:
    keypad.cleanup()			# Resteo pines del teclado
    lcd.clear()				# Limpio el display
    lcd.write_string('APP TERMINADA :)')
    lcd.cursor_pos = (1, 0)		# Cambio de linea
    lcd.write_string('FCEFyN-UNC->2019')
    time.sleep(1)			# Espero
    lcd.close(clear=True)		# Reseteo pines del display
    GPIO.cleanup()			# Reseteo el resto de los pines
